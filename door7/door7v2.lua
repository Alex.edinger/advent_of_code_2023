local file_handling = require("libs.file_handling")
local string_handling = require("libs.string_handling")
local table_handling = require("libs.table_handling")

local input_file = "door7\\input.txt"

local lines = file_handling.lines_from(input_file)

local rank_names = { "quintuple", "quadruple", "full_house", "triple", "double_pair", "single_pair", "high_card" }

local plays = {}
local ranks = {
	quintuple = {},
	quadruple = {},
	full_house = {},
	triple = {},
	double_pair = {},
	single_pair = {},
	high_card = {},
}

local descending_card_values = { "A", "K", "Q", "T", "9", "8", "7", "6", "5", "4", "3", "2", "J" }

local function is_card_higher_value(origin_card, compare_card)
	for i = 1, #origin_card do
		local position_of_origin_card_char = table_handling.get_element_position(descending_card_values,
			string.sub(origin_card, i, i))
		local position_of_compare_card_char = table_handling.get_element_position(descending_card_values,
			string.sub(compare_card, i, i))

		if position_of_origin_card_char < position_of_compare_card_char then
			return true
		elseif position_of_origin_card_char > position_of_compare_card_char then
			return false
		end
	end
	return true
end

local function get_rank_of_hand(hand)
	local appearance_of_numbers = {}
	local number_of_jokers = 0
	for i = 1, #hand do
		local char = string.sub(hand, i, i)

		if char == "J" then
			number_of_jokers = number_of_jokers + 1
		else
			if appearance_of_numbers[char] == nil then
				appearance_of_numbers[char] = 1
			else
				appearance_of_numbers[char] = appearance_of_numbers[char] + 1
			end
		end
	end

	local number_of_different_chars = table_handling.get_number_of_keys(appearance_of_numbers)

	if number_of_different_chars == 1 or number_of_different_chars == 0 then
		return "quintuple"
	elseif number_of_different_chars == 2 then
		-- AJJJ2 : quadruple
		-- AJAA2 : quadruple
		-- AJJA2 : quadruple
		-- AJA22 : full_house
		-- AAA22 : full_house
		-- AAAA2 : quadruple
		for _, value in pairs(appearance_of_numbers) do
			if value + number_of_jokers == 1 or value + number_of_jokers == 4 then
				return "quadruple"
			end
		end
		return "full_house"
	elseif number_of_different_chars == 3 then
		-- AA222 : triple
		-- AA223 : double_pair
		-- AA23J : triple
		-- A23JJ : triple
		for _, value in pairs(appearance_of_numbers) do
			if value + number_of_jokers == 3 then
				return "triple"
			end
		end
		return "double_pair"
	elseif number_of_different_chars == 4 then
		return "single_pair"
	else
		return "high_card"
	end
end

local function sort_play_to_ranks(hand, bid)
	local rank_of_play = get_rank_of_hand(hand)

	local inserted = false

	for i = 1, #ranks[rank_of_play] do
		if is_card_higher_value(hand, ranks[rank_of_play][i].hand) then
			table.insert(ranks[rank_of_play], i, { hand = hand, bid = bid })
			inserted = true
			break
		end
	end

	if not inserted then
		table.insert(ranks[rank_of_play], { hand = hand, bid = bid })
	end
end

for _, line_content in pairs(lines) do
	local parts = string_handling.get_individual_parts_of_string(line_content)

	table.insert(plays, { hand = parts[1], bid = parts[2] })
end


for _, play in pairs(plays) do
	sort_play_to_ranks(play.hand, play.bid)
end

local sum = 0
local rank = #lines
for _, rank_name in ipairs(rank_names) do
	for _, play in ipairs(ranks[rank_name]) do
		sum = sum + rank * play.bid
		rank = rank - 1
	end
end
print(sum)
