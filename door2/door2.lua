-- relative to the cmd
local file_handling = require("libs.file_handling")

local bag_contents = {
	red = 12,
	blue = 14,
	green = 13
}

local possible_games = {}
local fewest_cubes = {}

for line_number, content in pairs(file_handling.lines_from("door2\\input.txt")) do
	local is_possible = true
	fewest_cubes[line_number] = {}
	for colour, limit in pairs(bag_contents) do
		fewest_cubes[line_number][colour] = 0
		for capture in string.gmatch(content, "(%d*)[ ]" .. colour .. "") do
			if tonumber(capture) > limit then
				is_possible = false
			end
			if fewest_cubes[line_number][colour] < tonumber(capture) then
				fewest_cubes[line_number][colour] = tonumber(capture)
			end
		end
	end
	if is_possible then table.insert(possible_games, line_number) end
end

local sum = 0
for key, value in pairs(possible_games) do
	sum = sum + value
end
print(sum)

sum = 0
for game_id, colours in pairs(fewest_cubes) do
	sum = sum + colours["red"] * colours["blue"] * colours["green"]
end
print(sum)