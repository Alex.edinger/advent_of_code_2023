local file_handling = require("libs.file_handling")

local input_file = "door3\\input.txt"
local lines = file_handling.lines_from(input_file)

local candidate_numbers = {}

local function is_matched(i, pattern)
	if string.match(i, pattern) ~= nil then
		return true
	end
	return false
end

local function check_adjacent_tiles(line_number, column_number, pattern)
	local row_space = {}
	if line_number == 1 then
		row_space = { 1, 2 }
	elseif line_number == #lines then
		row_space = { #lines - 1, #lines }
	else
		row_space = { line_number - 1, line_number, line_number + 1 }
	end

	local col_space = {}
	if column_number == 1 then
		col_space = { 1, 2 }
	elseif column_number == #lines[1] then
		col_space = { #lines[1] - 1, #lines[1] }
	else
		col_space = { column_number - 1, column_number, column_number + 1 }
	end

	-- local temp_string = ""
	-- for _, value in pairs(row_space) do
	-- 	temp_string = temp_string .. " " .. value
	-- end
	-- print("rows")
	-- print(temp_string)

	-- temp_string = ""
	-- for _, value in pairs(col_space) do
	-- 	temp_string = temp_string .. " " .. value
	-- end
	-- print("cols")
	-- print(temp_string)

	for _, row in pairs(row_space) do
		for _, col in pairs(col_space) do
			if is_matched(string.sub(lines[row], col, col), pattern) then
				return true
			end
		end
	end
	return false
end

for line_number, line_content in pairs(lines) do
	-- local line_number = 1
	-- local line_content = lines[line_number]
	local current_number = ""

	local is_qualified = false
	for i = 1, #line_content do
		local c = string.sub(line_content, i, i)

		if string.match(c, "[0-9]") then
			current_number = current_number .. c
			if check_adjacent_tiles(tonumber(line_number), i, "[^0-9.]") then
				is_qualified = true
			end
		else
			if is_qualified then
				table.insert(candidate_numbers, tonumber(current_number))
				is_qualified = false
			end
			current_number = ""
		end
	end
	if is_qualified then
		table.insert(candidate_numbers, tonumber(current_number))
	end
end

local sum = 0
for _, qualify_number in pairs(candidate_numbers) do
	sum = sum + qualify_number
end
print(sum)