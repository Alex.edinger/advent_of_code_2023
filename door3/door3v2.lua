local file_handling = require("libs.file_handling")

local input_file = "door3\\input.txt"
local lines = file_handling.lines_from(input_file)

local candidate_numbers = {}

local function is_matched(i, pattern)
	if string.match(i, pattern) ~= nil then
		return true
	end
	return false
end

local function get_number_from_indice(indice)
	local column = indice[2]
	local current_number = ""
	while is_matched(string.sub(lines[indice[1]], column, column), "[0-9]") do
		column = column - 1
	end
	column = column + 1

	while is_matched(string.sub(lines[indice[1]], column, column), "[0-9]") do
		current_number = current_number .. string.sub(lines[indice[1]], column, column)
		column = column + 1
	end

	return tonumber(current_number)
end

local function get_indices_of_independent_numbers(line_number, column_number)
	local indizes = {}

	local row_space = {}
	if line_number == 1 then
		row_space = { 1, 2 }
	elseif line_number == #lines then
		row_space = { #lines - 1, #lines }
	else
		row_space = { line_number - 1, line_number, line_number + 1 }
	end

	local col_space = {}
	if column_number == 1 then
		col_space = { 1, 2 }
	elseif column_number == #lines[1] then
		col_space = { #lines[1] - 1, #lines[1] }
	else
		col_space = { column_number - 1, column_number, column_number + 1 }
	end

	for _, row in pairs(row_space) do
		for _, col in pairs(col_space) do
			if is_matched(string.sub(lines[row], col, col), "[0-9]") then
				if col == col_space[1] then
					table.insert(indizes, { row, col })
				else
					if is_matched(string.sub(lines[row], col - 1, col - 1), "[^0-9]") then
						table.insert(indizes, { row, col })
					-- else
					-- 	print("number to the left: " .. row .. " " .. col)
					end
				end
			-- else
			-- 	print("no number: " .. row .. " " .. col)
			end
		end
	end
	return indizes
end

--[[
]]
for line_number, line_content in pairs(lines) do
	for i = 1, #line_content do
		local c = string.sub(line_content, i, i)

		if string.match(c, "*") then
			local indices_of_adjacent_numbers = get_indices_of_independent_numbers(line_number, i)
			if #indices_of_adjacent_numbers == 2 then
				local constructed_numbers = {}
				for _, indice in pairs(indices_of_adjacent_numbers) do
					table.insert(constructed_numbers, get_number_from_indice(indice))
				end
				table.insert(candidate_numbers, constructed_numbers)
			end
		end
	end
end


-- for _, qualify_number in pairs(candidate_numbers) do
-- 	sum = sum + qualify_number
-- end
-- print(sum)

local sum = 0
for _, value in pairs(candidate_numbers) do
	sum =  sum + (value[1] * value[2])
end
print(sum)

-- for _, value in pairs(get_indices_of_independent_numbers(4, 107)) do
-- 	print(value[1] .. " " .. value[2])
-- end
