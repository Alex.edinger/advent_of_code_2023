-- relative to the cmd
local file_handling = require("libs.file_handling")

local replacement_table = {
-- common cases:	
	["o1e"]="one",
	["f4r"]="four",
	["f5e"]="five",
	["s6x"]="six",
	["s7n"]="seven",
	["t2o"]="two",
	["t3e"]="three",
	["n9e"]="nine",
	["e8t"]="eight"
}

local function get_number_from_line(line)

	for key, value in pairs(replacement_table) do
		line = string.gsub(line, value, key)
	end

	local first, last = string.match(line, "[^0-9]*(%d).*(%d)[^0-9]*")
	if first == nil then
		local first = string.match(line, ".*(%d).*")
		return line, tonumber(first..first)
	else
		return line, tonumber(first..last)
	end
end

-- relative to the cmd
local input_file = "door1\\input.txt"
local lines = file_handling.lines_from(input_file)

local sum = 0
for k,v in pairs(lines) do
	local adapted_line, result = get_number_from_line(v)
	sum = sum + result
	-- print(v.." - adapted: "..adapted_line..": ".." - result: "..result.." - sum: "..sum)
end
print(sum)