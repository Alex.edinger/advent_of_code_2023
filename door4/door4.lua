local file_handling = require("libs.file_handling")

local input_file = "door4\\input.txt"
local lines = file_handling.lines_from(input_file)

local winning_numbers = {}
local having_numbers = {}

for line_number, line_content in pairs(lines) do
	local col = string.find(line_content, ":") or 0

	local current_number = ""
	local temp_numbers = {}

	-- winning numbers
	while string.sub(line_content, col, col) ~= "|" do
		if string.match(string.sub(lines[line_number], col, col), "[0-9]") ~= nil then
			-- is a number
			current_number = current_number .. string.sub(lines[line_number], col, col)
		else
			if #current_number ~= 0 then
				table.insert(temp_numbers, tonumber(current_number))
			end
			current_number = ""
		end
		col = col + 1
	end
	winning_numbers[line_number] = temp_numbers

	temp_numbers = {}

	-- having numbers
	while col <= #line_content do
		if string.match(string.sub(line_content, col, col), "[0-9]") ~= nil then
			-- is a number
			current_number = current_number .. string.sub(lines[line_number], col, col)
		else
			if #current_number ~= 0 then
				table.insert(temp_numbers, tonumber(current_number))
			end
			current_number = ""
		end
		col = col + 1
	end

	if #current_number ~= 0 then
		table.insert(temp_numbers, tonumber(current_number))
	end
	having_numbers[line_number] = temp_numbers
end

-- local tempstring = ""
-- for _, value in pairs(winning_numbers[2]) do
-- 	tempstring = tempstring .. " " .. value
-- end

-- print("winning:")
-- print(tempstring)

-- tempstring = ""
-- for _, value in pairs(having_numbers[2]) do
-- 	tempstring = tempstring .. " " .. value
-- end
-- print("having:")
-- print(tempstring)

local sum = 0
for line_number, _ in pairs(lines) do
	local correct_cards = -1
	for _, winning_number in pairs(winning_numbers[line_number]) do
		
		for _, having_number in pairs(having_numbers[line_number]) do
			if having_number == winning_number then
				correct_cards = correct_cards + 1
				break
			end
		end
	end
	local addition = 0
	if correct_cards >= 0 then
		addition = 2^correct_cards
	end

	-- print(addition)
	sum = sum + addition
end
print(sum)