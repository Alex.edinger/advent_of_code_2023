local file_handling = require("libs.file_handling")

local input_file = "door4\\input.txt"
local lines = file_handling.lines_from(input_file)

local winning_numbers = {}
local having_numbers = {}

for line_number, line_content in pairs(lines) do
	local col = string.find(line_content, ":") or 0

	local current_number = ""
	local temp_numbers = {}

	-- winning numbers
	while string.sub(line_content, col, col) ~= "|" do
		if string.match(string.sub(lines[line_number], col, col), "[0-9]") ~= nil then
			-- is a number
			current_number = current_number .. string.sub(lines[line_number], col, col)
		else
			if #current_number ~= 0 then
				table.insert(temp_numbers, tonumber(current_number))
			end
			current_number = ""
		end
		col = col + 1
	end
	winning_numbers[line_number] = temp_numbers

	temp_numbers = {}

	-- having numbers
	while col <= #line_content do
		if string.match(string.sub(line_content, col, col), "[0-9]") ~= nil then
			-- is a number
			current_number = current_number .. string.sub(lines[line_number], col, col)
		else
			if #current_number ~= 0 then
				table.insert(temp_numbers, tonumber(current_number))
			end
			current_number = ""
		end
		col = col + 1
	end

	if #current_number ~= 0 then
		table.insert(temp_numbers, tonumber(current_number))
	end
	having_numbers[line_number] = temp_numbers
end

local card_amount = {}
for i = 1, #lines, 1 do
	card_amount[i] = 1
end


local sum = #lines
for line_number, _ in pairs(lines) do
	local correct_cards = 0
	for _, winning_number in pairs(winning_numbers[line_number]) do
		for _, having_number in pairs(having_numbers[line_number]) do
			if having_number == winning_number then
				correct_cards = correct_cards + 1
				break
			end
		end
	end

	print("line: " .. line_number)
	for i = 1, card_amount[line_number] do
		-- print("i: " .. i)
		sum = sum + correct_cards

		-- increase following cards
		for k = 1, correct_cards do
			local current_line = line_number + k
			if line_number + k <= #lines then
				-- print("current_line: " .. current_line)
				card_amount[current_line] = card_amount[current_line] + 1
			end
		end
	end
end

print(sum)
