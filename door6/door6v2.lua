local file_handling = require("libs.file_handling")
local string_handling = require("libs.string_handling")

local input_file = "door6\\input.txt"

local lines = file_handling.lines_from(input_file)

local times = string_handling.get_numbers_from_string(lines[1])
local distance_records = string_handling.get_numbers_from_string(lines[2])

local times_string = ""
local distance_record_string = ""

for i = 1, #times do
	times_string = times_string .. tostring(times[i])
	distance_record_string = distance_record_string .. tostring(distance_records[i])
end

local number_of_possibilites_for_game = 0

local current_time = tonumber(times_string)
local current_distance_record = tonumber(distance_record_string)

local calculated_distance = 0
local button_press_milli_seconds = 0

while calculated_distance <= current_distance_record do
	button_press_milli_seconds = button_press_milli_seconds + 1
	calculated_distance = button_press_milli_seconds * (current_time - button_press_milli_seconds)
end

print((current_time + 1) - (2*button_press_milli_seconds))