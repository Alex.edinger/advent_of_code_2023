local file_handling = require("libs.file_handling")
local string_handling = require("libs.string_handling")

local input_file = "door6\\input.txt"

local lines = file_handling.lines_from(input_file)

local times = string_handling.get_numbers_from_string(lines[1])
local distance_records = string_handling.get_numbers_from_string(lines[2])

local number_of_possibilites_for_game = {}

for i = 1, #times do 
	local current_time = times[i]
	local current_distance_record = distance_records[i]

	local calculated_distance = 0
	local button_press_milli_seconds = 0
	while calculated_distance <= current_distance_record do
		button_press_milli_seconds = button_press_milli_seconds + 1
		calculated_distance = button_press_milli_seconds * (current_time - button_press_milli_seconds)
	end

	table.insert(number_of_possibilites_for_game, ((current_time + 1) - (2*button_press_milli_seconds)))
end

local product = 1
for _, value in pairs(number_of_possibilites_for_game) do
	print(value)
	product = product * value
end
print(product)