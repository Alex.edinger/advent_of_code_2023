local file_handling = require("libs.file_handling")
local string_handling = require("libs.string_handling")

local input_file = "door5\\input.txt"
local lines = file_handling.lines_from(input_file)
local map_order = {}

local seeds_to_be_planted = {}

for _, number in pairs(string_handling.get_numbers_from_string(lines[1])) do
	table.insert(seeds_to_be_planted, number)
end

local sections = string_handling.get_sections(lines, "(.*) map:$", "^$")

for _, line_content in pairs(lines) do
	for section_header, _ in pairs(sections) do
		local regex_string = string.gsub(section_header, "-", "[-]")
		if string_handling.is_matched(line_content, regex_string) then
			table.insert(map_order, section_header)
		end
	end
end

-- for _, value in pairs(map_order) do
-- 	print(value)
-- end

-- for section_header, section_content in pairs(maps) do
-- 	print(section_header)
-- 	for _, content in pairs(section_content) do
-- 		print(content)
-- 	end
-- 	print("")
-- end

local function calculate_transformation(seed_number, transformation)
	print("input: " .. seed_number)
	-- transformation is the transformation by the map written in the maps
	-- seed_number is the current number of the seed number
	for _, content in pairs(sections[transformation]) do
		local numbers = string_handling.get_numbers_from_string(content)
		if numbers[1] == nil or numbers[2] == nil or numbers[3] == nil then
			print("error")
		else
			if seed_number >= numbers[2] and seed_number <= numbers[2] + numbers[3] then
				print(numbers[1] .. " " .. numbers[2] .. " " .. numbers[3])
				return numbers[1] + (seed_number - numbers[2])
			end
		end
	end
	print("return self")
	return seed_number
end

local locations = {}
for _, seed_value in pairs(seeds_to_be_planted) do
	local seed = seed_value
	for _, section_header in pairs(map_order) do
		seed = calculate_transformation(seed, section_header)
	end
	table.insert(locations, seed)
end


local lowest_location = locations[1]
for _, location in pairs(locations) do
	if location <= lowest_location then
		lowest_location = location
	end
end
print(lowest_location)