local file_handling = require("libs.file_handling")
local string_handling = require("libs.string_handling")

local input_file = "door5\\input.txt"

-- get lines from the input file
local lines = file_handling.lines_from(input_file)

-- store the order of the section inside the file, so the order of transfomations are known
local section_header_order = {}
-- store the number, until a change happens in a transformation and the transformation is no longer linear
local numbers_til_change = {}

-- get the sections from the file with the headers of the transformations
local sections = string_handling.get_sections(lines, "(.*) map:$", "^$")

-- iterate over the lines
for _, line_content in pairs(lines) do
	-- iterate over the sections
	for section_header, _ in pairs(sections) do
		-- substitute the "-"" in section string as to not cause regex problems
		local regex_string = string.gsub(section_header, "-", "[-]")

		-- check, if the sanitized section header exists in the file
		if string_handling.is_matched(line_content, regex_string) then
			-- insert it in the section header order
			table.insert(section_header_order, section_header)
		end
	end
end

---calculate the transformation for a seed number with a transformation name
---@param seed_number number
---@param transformation string
---@return number
local function calculate_transformation(seed_number, transformation)
	-- set the initial number very high, so it gets changed in the first step
	numbers_til_change[transformation] = 100000000000

	-- iterate over the indiviual section contents
	for _, content in pairs(sections[transformation]) do
		-- extract the numbers (should always be 3)
		local numbers = string_handling.get_numbers_from_string(content)

		-- print an error, if there aren't three numbers
		if numbers[1] == nil or numbers[2] == nil or numbers[3] == nil then
			print("error")
		else
			-- check if the seed is inside the transformation ROI
			if seed_number >= numbers[2] and seed_number < numbers[2] + numbers[3] then
				-- set the numbers until a change happens to the difference between the maximum and the seed number 
				numbers_til_change[transformation] = ((numbers[2] + numbers[3]) - seed_number)
				
				-- return the transformed seed number
				return
					numbers[1] + (seed_number - numbers[2])
			else -- seed outside the transformation ROI
				-- check, if the numbers_until_change can be updated, if the next transformation is lower than the current one
				if numbers_til_change[transformation] > numbers[2] - seed_number and seed_number < numbers[2] then
					-- set a new numbers_until_change with the range between the seed and the next transformation start
					numbers_til_change[transformation] = numbers[2] - seed_number
				end
			end
		end
	end
	-- no transformation happened. value is returned
	return seed_number
end

---get the lowest location from a seed
---@param current_seed number
---@return number
local function get_lowest_location(current_seed)
	-- declare the seed as a local variable so it can be changed
	local seed = current_seed
	
	-- iterate over the section_headers
	for _, section_header in pairs(section_header_order) do
		-- print("in seed: " .. seed)
		-- transform the seed
		seed = calculate_transformation(seed, section_header)
		-- print("out seed: " .. seed)
	end
	return seed
end

-- variable to store the lowest locations of seeds
local lowest_locations = {}

-- stores the current start of the pair of input seeds
local start_range = 0

-- iterate over the seeds
for index, number in pairs(string_handling.get_numbers_from_string(lines[1])) do
	if index % 2 == 0 then -- every second seed
		-- declare the start seeds_to_be_planted as the first seed
		local current_seed = start_range

		-- if the current seed is still inside the boundaries of the pair
		while current_seed < start_range + number do
			-- insert the lowest location of the current seed
			table.insert(lowest_locations, get_lowest_location(current_seed))

			-- variables to store the lowest skipping
			local skip_section_header = section_header_order[1]
			local lowest_until_number = numbers_til_change[skip_section_header]
			
			-- get the lowest skipping number
			for section_header, until_number in pairs(numbers_til_change) do
				if until_number < lowest_until_number then
					lowest_until_number = until_number
					skip_section_header = section_header
				end
			end

			-- print status
			print("current seed: " .. current_seed .. " - maximum: " .. start_range + number, " - skipping: " .. lowest_until_number .. " - on: " .. skip_section_header)
			
			-- update the current seed until a non linear transformation happens
			current_seed = current_seed + lowest_until_number
		end
	else
		start_range = number
	end
end

local lowest_location = lowest_locations[1]
for _, location in pairs(lowest_locations) do
	if location <= lowest_location then
		lowest_location = location
	end
end
print(lowest_location)