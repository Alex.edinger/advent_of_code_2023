local table_handling = {}

function table_handling.is_element_inside(array, element)
	for _, entry in pairs(array) do
		if entry == element then return true end
	end
	return false
end

function table_handling.get_element_position(array, element)
	for index, entry in ipairs(array) do
		if entry == element then return index end
	end
	return -1
end

function table_handling.get_number_of_keys(table)
	local number_of_keys = 0
	for _, _ in pairs(table) do
		number_of_keys = number_of_keys + 1
	end
	return number_of_keys
end

return table_handling
