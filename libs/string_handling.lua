local string_handling = {}

---get the numbers from a string
---@param number_string string: the string to extract the numbers from
---@return table: the numbers inside a lisz
function string_handling.get_numbers_from_string(number_string)
	local numbers = {}
	for number in string.gmatch(number_string, "%d+") do
		numbers[#numbers + 1] = tonumber(number)
	end
	return numbers
end

-- get the numbers from a string
function string_handling.get_individual_parts_of_string(parted_string)
	local parts = {}
	for part in string.gmatch(parted_string, "[ ]*([^ ]*)[ ]*") do
		parts[#parts + 1] = part
	end
	return parts
end

-- get the sections of a file according to a pattern
function string_handling.get_sections(lines, section_begin_pattern, section_end_pattern)
	local currently_in_section = false
	local sections = {}
	local section_header = ""
	local section_content = {}
	for line_number, line_content in pairs(lines) do
		if string.match(line_content, section_begin_pattern) then
			section_header = string.match(line_content, section_begin_pattern)
			currently_in_section = true
		elseif string.match(line_content, section_end_pattern) then
			if currently_in_section then
				sections[section_header] = section_content
				section_content = {}
				section_header = ""
				currently_in_section = false
			end
		else
			if currently_in_section then
				table.insert(section_content, line_content)
			end
		end
	end
	if currently_in_section then
		sections[section_header] = section_content
	end
	return sections
end

-- check whether a string is matched to the pattern
function string_handling.is_matched(i, pattern)
	if string.match(i, pattern) ~= nil then
		return true
	end
	return false
end

return string_handling
