local file_handling = {}

function file_handling.file_exists(file)
	local f = io.open(file, "rb")
	if f then f:close() end
	return f ~= nil
end

function file_handling.lines_from(file)
	if not file_handling.file_exists(file) then
		print("error")
		return {}
	end
	local lines = {}
	for line in io.lines(file) do
		lines[#lines + 1] = line
	end
	return lines
end

return file_handling
